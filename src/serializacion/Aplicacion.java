/*
Apuntes de Serializacion.
 */

package serializacion;

import java.io.*;

/**
 *
 * @author pcelardperez
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Creamos o ficheiro */
        File f = new File("datos_persoa");
        Escritura.escribir(f);
        Lectura.Leer(f);
       
    }
    
}
