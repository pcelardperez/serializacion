/*
Creamos a clase Escritura para escribir.
 */

package serializacion;

import java.io.*; /* Importamos la librería entera com .* */

/**
 *
 * @author pcelardperez
 */
public class Escritura {
    
    public static void escribir(File ficheiro){
        
        /* Abrimos el flujo para poder escribir */
        ObjectOutputStream fich =null;
        try{
            fich = new ObjectOutputStream(new FileOutputStream(ficheiro));
            /* Creamos las personas para no perder tiempo introduciendo los datos */
            /* Si tenemos un bucle hay que instanciar dentro del bucle */
            Persoa p1=new Persoa("aaa","111",999);
            Persoa p2=new Persoa("bbb", "222",888);
            Persoa p3=new Persoa("ccc","333",777);
            
            fich.writeObject(p1);
            fich.writeObject(p2);
            fich.writeObject(p3);
            
            
        }catch(IOException ex){
            System.out.println(" Error de escritura "+ex.getMessage());
        }finally{
            if(fich !=null)
                try{
                    fich.close();
                }catch(IOException ex){
                 System.out.println(" Error de escritura al cerrar el fichero"+ex.getMessage());
            }
        }
    }
}
