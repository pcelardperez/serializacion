/*
Clase Lectura para leer.
 */

package serializacion;

import java.io.*; /* Importamos toda la biblioteca */

/**
 *
 * @author pcelardperez
 */
public class Lectura {
    
    public static void Leer(File ficheiro){
        
        ObjectInputStream fich=null;
        
        try{
         fich = new ObjectInputStream(new FileInputStream(ficheiro));
         Persoa p=(Persoa)fich.readObject();
         while(p!=null){
             System.out.println(p);
             p=(Persoa)fich.readObject(); /* No se pone otra vez Persoa p porque ya está declarada arriba */
         }
        }catch(Exception ex){
            System.out.println(" Error de lectura "+ex.getMessage());
        }finally{
            if(fich != null){
                try{
                    fich.close(); 
                }catch(IOException ex){
                    System.out.println(" Error de lectura al cerrar el fichero"+ex.getMessage());
                }
            }
            
        }
        
    }
}
